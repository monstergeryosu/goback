//
//  SecondViewController.swift
//  goback
//
//  Created by Chise Miyagi on 2019/04/27.
//  Copyright © 2019 Chise Miyagi. All rights reserved.
//

import UIKit

class SecondViewController: UIViewController {
    
    @IBOutlet var closebtn:UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func tapCloseBtn(){
        
        dismiss(animated: true)
    }

    

}
